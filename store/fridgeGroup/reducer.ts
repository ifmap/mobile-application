import {
    FridgeGroupState,
    FridgeGroupActionTypes,
    ADD_FRIDGE_GROUP,
    UPDATE_FRIDGE_GROUP,
    REMOVE_FRIDGE_GROUP,
    SET_FRIDGE_GROUPS,
    REMOVE_LOADING,
    ADD_LOADING
} from './types';

const initialState: FridgeGroupState = {
    fridgeGroups: [],
    loading: 0
};

export default (state = initialState, action: FridgeGroupActionTypes): FridgeGroupState => {
    switch (action.type) {
        case ADD_FRIDGE_GROUP:
            return {
                ...state,
                fridgeGroups: [...state.fridgeGroups, action.fridgeGroup]
            };
        case UPDATE_FRIDGE_GROUP:

            return {
                ...state,
                fridgeGroups: [...state.fridgeGroups.filter(item => item.id !== action.updatedFridgeGroup.id), action.updatedFridgeGroup]
            };
        case REMOVE_FRIDGE_GROUP:
            return {
                ...state,
                fridgeGroups: state.fridgeGroups.filter(item => item.id !== action.id)
            };
        case SET_FRIDGE_GROUPS:
            return {
                ...state,
                fridgeGroups: [{ id: -1, name: "Uncategorized" }, ...action.fridgeGroups]
            };
        case ADD_LOADING:
            return {
                ...state,
                loading: state.loading + 1
            };
        case REMOVE_LOADING:
            return {
                ...state,
                loading: state.loading - 1
            };
        default:
            return state;
    }
};