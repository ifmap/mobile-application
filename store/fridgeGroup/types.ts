export const ADD_FRIDGE_GROUP = 'ADD_FRIDGE_GROUP';
export const UPDATE_FRIDGE_GROUP = 'UPDATE_FRIDGE_GROUP';
export const REMOVE_FRIDGE_GROUP = 'REMOVE_FRIDGE_GROUP';
export const SET_FRIDGE_GROUPS = 'SET_FRIDGE_GROUPS';
export const ADD_LOADING = 'ADD_LOADING';
export const REMOVE_LOADING = 'REMOVE_LOADING';

interface AddFridgeGroupAction {
    type: typeof ADD_FRIDGE_GROUP;
    fridgeGroup: FridgeGroup;
}

interface UpdateFridgeGroupAction {
    type: typeof UPDATE_FRIDGE_GROUP;
    updatedFridgeGroup: FridgeGroup;
}

interface RemoveFridgeGroupAction {
    type: typeof REMOVE_FRIDGE_GROUP;
    id: number;
}

interface SetFridgeGroupsAction {
    type: typeof SET_FRIDGE_GROUPS;
    fridgeGroups: FridgeGroup[];
}

interface AddLoadingAction {
    type: typeof ADD_LOADING;
}

interface RemoveLoadingAction {
    type: typeof REMOVE_LOADING;
}

export type FridgeGroupActionTypes = AddFridgeGroupAction | UpdateFridgeGroupAction | RemoveFridgeGroupAction | SetFridgeGroupsAction | AddLoadingAction | RemoveLoadingAction;

export interface FridgeGroup {
    id?: number;
    name: string;
}

export interface FridgeGroupState {
    fridgeGroups: FridgeGroup[];
    loading: number;
}