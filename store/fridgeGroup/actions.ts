import { Alert } from 'react-native';
import { Dispatch, AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';
import * as firebase from 'firebase';
import "firebase/auth";
import "firebase/firestore";

import {
  FridgeGroup,
  FridgeGroupState,
  FridgeGroupActionTypes,
  ADD_FRIDGE_GROUP,
  UPDATE_FRIDGE_GROUP,
  REMOVE_FRIDGE_GROUP,
  SET_FRIDGE_GROUPS,
  REMOVE_LOADING,
  ADD_LOADING
} from './types';
import { FridgeItem } from '../fridge/types';
import { setFridgeItems } from '../fridge/actions';

const addFridgeGroup = (fridgeGroup: FridgeGroup): FridgeGroupActionTypes => ({
  type: ADD_FRIDGE_GROUP,
  fridgeGroup
});

const updateFridgeGroup = (updatedFridgeGroup: FridgeGroup): FridgeGroupActionTypes => ({
  type: UPDATE_FRIDGE_GROUP,
  updatedFridgeGroup
});

const removeFridgeGroup = (id: number): FridgeGroupActionTypes => ({
  type: REMOVE_FRIDGE_GROUP,
  id
});

export const setFridgeGroups = (fridgeGroups: FridgeGroup[]): FridgeGroupActionTypes => ({
  type: SET_FRIDGE_GROUPS,
  fridgeGroups
});

export const addLoading = (): FridgeGroupActionTypes => ({ type: ADD_LOADING });

export const removeLoading = (): FridgeGroupActionTypes => ({type: REMOVE_LOADING});

export const addFridgeGroupAsync = (fridgeGroup: FridgeGroup): ThunkAction<Promise<FridgeGroupActionTypes | void>, FridgeGroupState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeGroupActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeGroups = UserData?.fridgeGroups ?? [];
        const nextFridgeGroupId = UserData?.nextFridgeGroupId ?? 1;
        fridgeGroup.id = nextFridgeGroupId;
        await UserDocRef.set({
          fridgeGroups: [...fridgeGroups, fridgeGroup],
          nextFridgeGroupId: nextFridgeGroupId + 1
        }, { merge: true });
        dispatch(addFridgeGroup(fridgeGroup));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to add a fridge group: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => addFridgeGroupAsync(fridgeGroup)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const updateFridgeGroupAsync = (fridgeGroup: FridgeGroup): ThunkAction<Promise<FridgeGroupActionTypes | void>, FridgeGroupState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeGroupActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeGroups = UserData?.fridgeGroups ?? [];
        await UserDocRef.set({
          fridgeGroups: [...fridgeGroups.filter((I: FridgeGroup) => I.id != fridgeGroup.id), fridgeGroup],
        }, { merge: true });
        dispatch(updateFridgeGroup(fridgeGroup));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to update a fridge group: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => updateFridgeGroupAsync(fridgeGroup)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const removeFridgeGroupOnlyAsync = (id: number): ThunkAction<Promise<FridgeGroupActionTypes | void>, FridgeGroupState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeGroupActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeItems = UserData?.fridgeItems ?? [];
        const fridgeGroups = UserData?.fridgeGroups ?? [];
        const newFridgeItems = fridgeItems.map((I: FridgeItem) => {
          if (I.fridgeGroupId === id) { delete I.fridgeGroupId; }
          return I;
        });
        await UserDocRef.set({
          fridgeGroups: fridgeGroups.filter((I: FridgeGroup) => I.id !== id),
          fridgeItems: newFridgeItems,
        }, { merge: true });
        dispatch(removeFridgeGroup(id));
        dispatch(setFridgeItems(newFridgeItems));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to remove a fridge group: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => removeFridgeGroupOnlyAsync(id)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const removeFridgeGroupAndItemsAsync = (id: number): ThunkAction<Promise<FridgeGroupActionTypes | void>, FridgeGroupState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeGroupActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeItems = UserData?.fridgeItems ?? [];
        const fridgeGroups = UserData?.fridgeGroups ?? [];
        const newFridgeItems = fridgeItems.filter((I: FridgeItem) => I.fridgeGroupId !== id);
        await UserDocRef.set({
          fridgeGroups: fridgeGroups.filter((I: FridgeGroup) => I.id !== id),
          fridgeItems: newFridgeItems,
        }, { merge: true });
        dispatch(removeFridgeGroup(id));
        dispatch(setFridgeItems(newFridgeItems));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to remove a fridge group: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => removeFridgeGroupAndItemsAsync(id)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}