import { Alert } from 'react-native';
import { Dispatch, AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';
import * as firebase from 'firebase';
import "firebase/auth";
import "firebase/firestore";

import { addNotification, restoresRemindersAsync } from '../../utils/notifications';
import {
  ReminderState,
  ReminderActionTypes,
  Reminder,
  ADD_REMINDER,
  REMOVE_REMINDER,
  SET_REMINDERS,
  SET_GLOBAL_REMINDERS,
  REMOVE_LOADING,
  ADD_LOADING
} from './types';

const addReminder = (reminder: Reminder): ReminderActionTypes => ({
  type: ADD_REMINDER,
  reminder
});

export const removeReminder = (id: number): ReminderActionTypes => ({
  type: REMOVE_REMINDER,
  id
});

export const setReminders = (reminders: Reminder[]): ReminderActionTypes => ({
  type: SET_REMINDERS,
  reminders
});

export const setGlobalReminders = (globalReminders: Reminder[]): ReminderActionTypes => ({
  type: SET_GLOBAL_REMINDERS,
  globalReminders
});

const addLoading = (): ReminderActionTypes => ({ type: ADD_LOADING });

const removeLoading = (): ReminderActionTypes => ({type: REMOVE_LOADING});

export const addReminderAsync = (reminder: Reminder): ThunkAction<Promise<ReminderActionTypes | void>, ReminderState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<ReminderActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const reminders = UserData?.reminders ?? [];
        const nextReminderId = UserData?.nextReminderId ?? 1;
        reminder.id = nextReminderId;
        await UserDocRef.set({
          reminders: [...reminders, reminder],
          nextReminderId: nextReminderId + 1
        }, { merge: true });
        dispatch(addReminder(reminder));
        // Add Notification
        await addNotification("IFMAP Custom Reminder", reminder.title ?? "A fruit's reminder has been triggered", (reminder.sentDate - Date.now()) / 1000);
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while trying to add a reminder: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => addReminderAsync(reminder)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const removeReminderAsync = (id: number): ThunkAction<Promise<ReminderActionTypes | void>, ReminderState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<ReminderActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const reminders = UserData?.reminders ?? [];
        const newReminders = reminders.filter((R: Reminder) => R.id != id);
        await UserDocRef.set({
          reminders: newReminders
        }, { merge: true });
        dispatch(removeReminder(id));
        // Syncs Notifications
        await restoresRemindersAsync(newReminders);
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to remove a reminder: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => removeReminderAsync(id)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}