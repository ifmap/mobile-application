export const ADD_REMINDER = 'ADD_REMINDER';
export const REMOVE_REMINDER = 'REMOVE_REMINDER';
export const SET_REMINDERS = 'SET_REMINDERS';
export const SET_GLOBAL_REMINDERS = 'SET_GLOBAL_REMINDERS';
export const ADD_LOADING = 'ADD_LOADING';
export const REMOVE_LOADING = 'REMOVE_LOADING';

interface AddReminderAction {
    type: typeof ADD_REMINDER;
    reminder: Reminder;
}

interface RemoveReminderAction {
    type: typeof REMOVE_REMINDER;
    id: number;
}

interface SetRemindersAction {
    type: typeof SET_REMINDERS;
    reminders: Reminder[];
}

interface SetGlobalRemindersAction {
    type: typeof SET_GLOBAL_REMINDERS;
    globalReminders: Reminder[];
}

interface AddLoadingAction {
    type: typeof ADD_LOADING;
}

interface RemoveLoadingAction {
    type: typeof REMOVE_LOADING;
}

export type ReminderActionTypes = AddReminderAction | RemoveReminderAction | SetRemindersAction | SetGlobalRemindersAction | AddLoadingAction | RemoveLoadingAction

export interface Reminder {
    id?: number;
    fridgeItemId?: number;
    title?: string;
    type: ItemType;
    sentDate: number;
    expirationDate?: number;
    body?: string;
    severity: number;
    isDeletable: boolean;
}

export interface ReminderState {
    reminders: Reminder[];
    globalReminders: Reminder[];
    loading: number;
}

export type ItemType = "Warning" | "Danger" | "Reminder" | "Info" | ""