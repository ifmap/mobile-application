import {
    ReminderState,
    ReminderActionTypes,
    ADD_REMINDER,
    REMOVE_REMINDER,
    SET_REMINDERS,
    SET_GLOBAL_REMINDERS,
    REMOVE_LOADING,
    ADD_LOADING
} from './types';

const initialState: ReminderState = {
    reminders: [],
    globalReminders: [],
    loading: 0,
};

export default (state = initialState, action: ReminderActionTypes): ReminderState => {
    switch (action.type) {
        case ADD_REMINDER:
            return {
                ...state,
                reminders: [...state.reminders, action.reminder]
            };
        case REMOVE_REMINDER:
            return {
                ...state,
                reminders: state.reminders.filter(item => item.id !== action.id)
            };
        case SET_REMINDERS:
            return {
                ...state,
                reminders: action.reminders
            };
        case SET_GLOBAL_REMINDERS:
            return {
                ...state,
                globalReminders: action.globalReminders
            };
        case ADD_LOADING:
            return {
                ...state,
                loading: state.loading + 1
            };
        case REMOVE_LOADING:
            return {
                ...state,
                loading: state.loading - 1
            };
        default:
            return state;
    }
};