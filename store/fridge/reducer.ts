import {
    FridgeState,
    FridgeActionTypes,
    ADD_FRIDGE_ITEM,
    UPDATE_FRIDGE_ITEM,
    REMOVE_FRIDGE_ITEM,
    SET_FRIDGE_ITEMS,
    REMOVE_LOADING,
    ADD_LOADING
} from './types';

const initialState: FridgeState = {
    fridgeItems: [],
    loading: 0,
};

export default (state = initialState, action: FridgeActionTypes): FridgeState => {
    switch (action.type) {
        case ADD_FRIDGE_ITEM:
            return {
                ...state,
                fridgeItems: [...state.fridgeItems, action.fridgeItem]
            };
        case UPDATE_FRIDGE_ITEM:

            return {
                ...state,
                fridgeItems: [...state.fridgeItems.filter(item => item.id !== action.updateFridgeItem.id), action.updateFridgeItem]
            };
        case REMOVE_FRIDGE_ITEM:
            return {
                ...state,
                fridgeItems: state.fridgeItems.filter(item => item.id !== action.id)
            };
        case SET_FRIDGE_ITEMS:
            return {
                ...state,
                fridgeItems: action.fridgeItems
            };
            case ADD_LOADING:
                return {
                    ...state,
                    loading: state.loading + 1
                };
            case REMOVE_LOADING:
                return {
                    ...state,
                    loading: state.loading - 1
                };
        default:
            return state;
    }
};