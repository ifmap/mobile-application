export const ADD_FRIDGE_ITEM = 'ADD_FRIDGE_ITEM';
export const UPDATE_FRIDGE_ITEM = 'UPDATE_FRIDGE_ITEM';
export const REMOVE_FRIDGE_ITEM = 'REMOVE_FRIDGE_ITEM';
export const SET_FRIDGE_ITEMS = 'SET_FRIDGE_ITEMS';
export const ADD_LOADING = 'ADD_LOADING';
export const REMOVE_LOADING = 'REMOVE_LOADING';

interface AddFridgeItemAction {
    type: typeof ADD_FRIDGE_ITEM;
    fridgeItem: FridgeItem;
}

interface UpdateFridgeItemAction {
    type: typeof UPDATE_FRIDGE_ITEM;
    updateFridgeItem: FridgeItem;
}

interface RemoveFridgeItemAction {
    type: typeof REMOVE_FRIDGE_ITEM;
    id: number;
}

interface SetFridgeItemsAction {
    type: typeof SET_FRIDGE_ITEMS;
    fridgeItems: FridgeItem[];
}

interface AddLoadingAction {
    type: typeof ADD_LOADING;
}

interface RemoveLoadingAction {
    type: typeof REMOVE_LOADING;
}

export type FridgeActionTypes = AddFridgeItemAction | UpdateFridgeItemAction | RemoveFridgeItemAction | SetFridgeItemsAction | AddLoadingAction | RemoveLoadingAction;

export interface FridgeItem {
    id?: number;
    fridgeGroupId?: number;
    name: string;
    type: ItemType;
    date?: number;
    expiryDate?: number;
    status?: string;
    note?: string;
}

export interface FridgeState {
    fridgeItems: FridgeItem[];
    loading: number;
}

export type ItemType = "Scanned" | "Manual"