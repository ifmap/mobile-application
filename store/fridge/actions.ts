import { Alert } from 'react-native';
import { Dispatch, AnyAction } from 'redux';
import { ThunkAction } from 'redux-thunk';
import * as firebase from 'firebase';
import "firebase/auth";
import "firebase/firestore";

import { removeReminder } from '../reminder/actions';
import {
  FridgeItem,
  FridgeState,
  FridgeActionTypes,
  ADD_FRIDGE_ITEM,
  UPDATE_FRIDGE_ITEM,
  REMOVE_FRIDGE_ITEM,
  SET_FRIDGE_ITEMS,
  REMOVE_LOADING,
  ADD_LOADING
} from './types';
import { Reminder } from '../reminder/types';

const addFridgeItem = (fridgeItem: FridgeItem): FridgeActionTypes => ({
  type: ADD_FRIDGE_ITEM,
  fridgeItem
});

export const updateFridgeItem = (updateFridgeItem: FridgeItem): FridgeActionTypes => ({
  type: UPDATE_FRIDGE_ITEM,
  updateFridgeItem
});

export const removeFridgeItem = (id: number): FridgeActionTypes => ({
  type: REMOVE_FRIDGE_ITEM,
  id
});

export const setFridgeItems = (fridgeItems: FridgeItem[]): FridgeActionTypes => ({
  type: SET_FRIDGE_ITEMS,
  fridgeItems
});

const addLoading = (): FridgeActionTypes => ({ type: ADD_LOADING });

const removeLoading = (): FridgeActionTypes => ({type: REMOVE_LOADING});

export const addFridgeItemAsync = (fridgeItem: FridgeItem): ThunkAction<Promise<FridgeActionTypes | void>, FridgeState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeItems = UserData?.fridgeItems ?? [];
        const nextFridgeItemId = UserData?.nextFridgeItemId ?? 1;
        fridgeItem.id = nextFridgeItemId;
        await UserDocRef.set({
          fridgeItems: [...fridgeItems, fridgeItem],
          nextFridgeItemId: nextFridgeItemId + 1
        }, { merge: true });
        dispatch(addFridgeItem(fridgeItem));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to add a fridge item: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => addFridgeItemAsync(fridgeItem)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const updateFridgeItemAsync = (fridgeItem: FridgeItem): ThunkAction<Promise<FridgeActionTypes | void>, FridgeState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeItems = UserData?.fridgeItems ?? [];
        await UserDocRef.set({
          fridgeItems: [...fridgeItems.filter((I: FridgeItem) => I.id != fridgeItem.id), fridgeItem]
        }, { merge: true });
        dispatch(updateFridgeItem(fridgeItem));
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to update a fridge item: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => updateFridgeItemAsync(fridgeItem)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}

export const removeFridgeItemAsync = (id: number): ThunkAction<Promise<FridgeActionTypes | void>, FridgeState, unknown, AnyAction> => {
  return async (dispatch: Dispatch): Promise<FridgeActionTypes | void> => {
    dispatch(addLoading());
    try {
      const db = firebase.firestore();
      const auth = firebase.auth();
      if (auth.currentUser) {
        const UserDocRef = db.collection("Users").doc(auth.currentUser.uid);
        const UserData = (await UserDocRef.get()).data();
        const fridgeItems = UserData?.fridgeItems ?? [];
        const reminders = UserData?.reminders ?? [];
        await UserDocRef.set({
          fridgeItems: fridgeItems.filter((I: FridgeItem) => I.id != id),
          reminders: reminders.filter((R: Reminder) => R.fridgeItemId != id),
        }, { merge: true });
        // Update state (including reminders of the deleted item)
        dispatch(removeFridgeItem(id));
        reminders.forEach((reminder: Reminder): void => {
          if (reminder.id != null && reminder.fridgeItemId === id) {
            dispatch(removeReminder(reminder.id));
          }
        });
      }
    } catch (err) {
      Alert.alert(
        'Error',
        "The following error occured while to remove a fridge item: " + err,
        [
          { text: 'OK' },
          { text: 'TRY AGAIN', onPress: () => removeFridgeItemAsync(id)}
        ]
      );
    }
    dispatch(removeLoading());
  }
}