import { Alert } from 'react-native';
import * as firebase from 'firebase';
import "firebase/auth";
import "firebase/firestore";

import { setGlobalReminders, setReminders } from './reminder/actions';
import { setFridgeItems } from './fridge/actions';
import { setFridgeGroups, addLoading, removeLoading } from './fridgeGroup/actions';
import { UserDataObject } from '../utils/types';
import { restoresRemindersAsync } from '../utils/notifications';
import { Dispatch } from 'redux';

// Retrieve user specific data
export const initStoreUserData = async (dispatch: Dispatch): Promise<void> => { 
  try {
    dispatch(addLoading());
    const db = firebase.firestore();
    const auth = firebase.auth();
    if (auth.currentUser) {
      const UserDoc = await db.collection("Users").doc(auth.currentUser.uid).get();
      const UserData: UserDataObject = UserDoc.data() ?? {};
      await parseUserData(dispatch, UserData);
    }
  } catch (err) {
    Alert.alert(
      'Error',
      "The following error occured while retrieving your data: " + err,
      [
        { text: 'OK' },
        { text: 'TRY AGAIN', onPress: () => initStoreUserData(dispatch)}
      ]
    );
  }
  dispatch(removeLoading());
}

// Store data in the various stores
export const parseUserData = async (dispatch: Dispatch, UserData: UserDataObject): Promise<void> => { 
  try {
    const auth = firebase.auth();
    if (auth.currentUser) {
      const reminders = UserData?.reminders ?? [];
      const fridgeItems = UserData?.fridgeItems ?? [];
      const fridgeGroups = UserData?.fridgeGroups ?? [];
      dispatch(setReminders(reminders));
      dispatch(setFridgeItems(fridgeItems));
      dispatch(setFridgeGroups(fridgeGroups));
      // Syncs notifications (on Android, local notifications are deleted on restart. This also syncs in case a user uses multiple devices)
      await restoresRemindersAsync(reminders);
    }
  } catch (err) {
    Alert.alert(
      'Error',
      "The following error occured while parsing your data: " + err,
      [
        { text: 'OK' },
        { text: 'TRY AGAIN', onPress: () => parseUserData(dispatch, UserData)}
      ]
    );
  }
}

// Retrieve data to be used regardless of sign-in status
export const initStoreGlobalData = async (dispatch: Dispatch): Promise<void> => { 
  try {
    const db = firebase.firestore();
    const GlobalData = await db.collection("Global").doc("Info").get();
    const globalReminders = GlobalData.data()?.Notifications ?? [];
    dispatch(setGlobalReminders(globalReminders));
  } catch (err) {
    Alert.alert(
      'Error',
      "The following error occured while retrieving global data: " + err,
      [
        { text: 'OK' },
        { text: 'TRY AGAIN', onPress: () => initStoreGlobalData(dispatch) }
      ]
    );
  }
}