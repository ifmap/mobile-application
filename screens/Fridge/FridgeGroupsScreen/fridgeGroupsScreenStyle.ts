import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    icon: {
        alignSelf: 'center',
        color: Colors.notifBlue,
        borderRadius: 35,
        marginTop: 20,
        opacity: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    title: {
        color: Colors.notifBlue,
        fontSize: 30,
    }
});
