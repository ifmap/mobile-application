import React, { useEffect, useCallback } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import HeaderButton from '../../../components/ui/HeaderButton/HeaderButton';
import styles from './fridgeGroupsScreenStyle';
import { RootState } from '../../../App';
import FridgeGroup from '../../../components/fridgescreen/FridgeGroup/FridgeGroup';
import { initStoreUserData } from '../../../store/Helpers';
import { ActivityIndicator } from 'react-native';

/** Renders the page with all the fridge options */
const FridgeGroupsScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const dispatch = useDispatch();
  const fridgeGroups = useSelector((state: RootState) => state.fridgeGroups.fridgeGroups);
  const loadState = useSelector((state: RootState) => state.fridgeGroups.loading);

  const loadData = useCallback(() => {
    initStoreUserData(dispatch);
  }, [dispatch]);
  
  // Passes the load data function to the header icons
  useEffect(() => {
    navigation.setParams({onPressRefreshButton: loadData});
  }, []);
  
  return (
    <View style={styles.content}>
      { loadState > 0 &&
        <View>
          <ActivityIndicator style={styles.icon} size="large" />
        </View>
      }
      <Text style={styles.title}>Categories:</Text>
      <FlatList
        data={fridgeGroups}
        keyExtractor={(n) => (`${n.id}`)}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Fridge', { id: item.id })}
          >
            <FridgeGroup id={item.id} name={item.name} />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

FridgeGroupsScreen.navigationOptions = (navData: NavigationParams) => ({
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <>
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Refresh"
        iconName="ios-refresh"
        onPress={navData.navigation.getParam('onPressRefreshButton')}
      />
    </HeaderButtons>
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Add group"
          iconName="ios-add-circle"
          onPress={() => {
            navData.navigation.navigate('AddFridgeGroup');
          }}
        />
      </HeaderButtons>
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Add Item"
          iconName="ios-egg"
          onPress={() => {
            navData.navigation.navigate('AddFridgeItem');
          }}
        />
      </HeaderButtons>
    </>
  ),
});

export default FridgeGroupsScreen;
