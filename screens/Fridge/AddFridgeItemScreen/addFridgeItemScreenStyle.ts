import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    buttons: {
        height: 350,
        justifyContent: 'space-between',
        width: 250,
    },
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    error: {
        color: Colors.error,
    },
    picker: {
        width: 250,
    },
});