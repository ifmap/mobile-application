import React, { useState } from 'react';
import {
  View, Button, TextInput, Text, Picker
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationParams } from 'react-navigation';
import DateTimePicker from '@react-native-community/datetimepicker';

import { RootState } from '../../../App';
import styles from './addFridgeItemScreenStyle';
import { addFridgeItemAsync } from '../../../store/fridge/actions';

/** Renders the sign up screen. */
const AddFridgeItemScreen: React.FC<NavigationParams> = ({ navigation }) => {
  const dispatch = useDispatch();

  const fridgeGroups = useSelector((state: RootState) => state.fridgeGroups.fridgeGroups);
  const [name, setName] = useState<string | undefined>();
  const [expiryDate, setExpiryDate] = useState<Date>(new Date());
  const [show, setShow] = useState<boolean>(false);
  const [groupId, setGroupId] = useState<number>(-1);

  let hasNameErrors = false;
  let hasDateErrors = false;

  const onChange = (event: Event, selectedDate: Date | undefined) => {
    const currentDate = selectedDate || expiryDate;
    setShow(false);
    setExpiryDate(currentDate);
  };

  if (!name) {
    hasNameErrors = true;
  }

  if (!expiryDate || expiryDate.valueOf() < Date.now()) {
    hasDateErrors = true;
  }

  const renderGroupPicker = () => { 
    if (fridgeGroups.length > 1) { 
      return (
        <Picker
          style={styles.picker} 
          selectedValue={groupId}
          onValueChange={(itemValue) => setGroupId(itemValue)}
        >
          {fridgeGroups.map((G, i) => <Picker.Item key={i} label={G.name} value={G.id} />)}
        </Picker>
      );
    } 
    return <Text>You have no existing group. Create a fridge group to add this item to a group.</Text>
  }
  
  const addPress = (): void => {
    if (name && expiryDate) {
      dispatch(addFridgeItemAsync({ name, expiryDate: expiryDate.valueOf(), type: 'Manual', fridgeGroupId: groupId }));
      navigation.navigate('FridgeGroup');
    }
  }

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <TextInput
          placeholder="Name"
          onChangeText={(value) => setName(value)}
        />
        {
          hasNameErrors &&
          <Text style={styles.error}>Please enter a valid name.</Text>
        }
        <View>
          <Button onPress={() => setShow(true)} title={expiryDate.toDateString() || "Select a date"} />
        </View>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={expiryDate}
            mode='date'
            onChange={onChange}
          />
        )}
        {
          hasDateErrors &&
          <Text style={styles.error}>Please enter a valid date.</Text>
        }
        {renderGroupPicker()}
        <Button // modifier pour faire un bouton de redirection
          title="Add item to fridge"
          onPress={addPress}
          disabled={hasDateErrors || hasNameErrors}
        />
      </View>
    </View>

  );
};

export default AddFridgeItemScreen;
