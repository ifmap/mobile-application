import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between',
        marginTop: 10,
        paddingLeft: 20,
    },
    icon: {
        alignSelf: 'center',
        color: Colors.notifBlue,
        borderRadius: 35,
        marginTop: 20,
        opacity: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    picker: {
        width: 250,
    },
    reminders: {
      marginTop: 20  
    },
    textInfo: {
        color: Colors.accent,
        fontSize: 24,
    }
});