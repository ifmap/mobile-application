import React, { useState } from 'react';
import {
  View,
  Text,
  Button,
  Picker,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';
import { TextInput } from 'react-native-gesture-handler';

import HeaderButton from '../../../components/ui/HeaderButton/HeaderButton';
import ReminderItem from '../../../components/fridgescreen/ReminderItem/ReminderItem';
import styles from './fridgeItemScreenStyle';
import { RootState } from '../../../App';
import { FridgeItem } from '../../../store/fridge/types';
import { updateFridgeItemAsync } from '../../../store/fridge/actions';

//TODO: display image

/** Renders the page with all the fridge options */
const FridgeItemScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const dispatch = useDispatch();
  const fridgeGroups = useSelector((state: RootState) => state.fridgeGroups.fridgeGroups);
  const reminders = useSelector((state: RootState) => state.reminders.reminders);
  const loadState = useSelector((state: RootState) => state.reminders.loading);
  const fridgeItem: FridgeItem = navigation.getParam('item');
  const [groupId, setGroupId] = useState<number | undefined>(fridgeItem.fridgeGroupId);
  const [noteValue, setNoteValue] = useState<string>('');

  const saveFridgeItem = () => {
    fridgeItem.note = noteValue;
    fridgeItem.fridgeGroupId = groupId;
    dispatch(updateFridgeItemAsync(fridgeItem));
  }
  
  const renderGroupPicker = () => {
    if (fridgeGroups.length > 1) {
      return (
        <Picker
          style={styles.picker}
          selectedValue={groupId}
          onValueChange={(itemValue) => setGroupId(itemValue)}
        >
          {fridgeGroups.map((G, i) => <Picker.Item key={i} label={G.name} value={G.id} />)}
        </Picker>
      );
    }
    return <Text>You have no existing group. Create a fridge group to add this item to a group.</Text>
  }
  
  return (
    <View style={styles.content}>
      <View>
        <Text style={styles.textInfo}>Item: {fridgeItem.type} - {fridgeItem.name}</Text>
        {fridgeItem.date &&
          <Text style={styles.textInfo}>
            Date of analysis: {new Date(fridgeItem.date).toLocaleDateString()}
          </Text>
        }
        {fridgeItem.status &&
          <Text style={styles.textInfo}>Status when analyzed: {fridgeItem.status}</Text>
        }
        {fridgeItem.expiryDate &&
          <Text style={styles.textInfo}>
            Expiration date: {new Date(fridgeItem.expiryDate).toLocaleDateString()}
          </Text>
        }
        <Text style={styles.textInfo}>Category: </Text>
        {renderGroupPicker()}
      </View>
      <View>
        <TextInput
          multiline={true}
          numberOfLines={4}
          onChangeText={setNoteValue}
          placeholder={fridgeItem.note || 'Enter a note.'}
        />
      </View>
      <View>
        <Button onPress={saveFridgeItem} title="Save changes" />
      </View>
      { loadState > 0 &&
        <View>
          <ActivityIndicator style={styles.icon} size="large" />
        </View>
      }
      <View style={styles.reminders}>
        <FlatList
          data={reminders.filter(R => R.fridgeItemId === fridgeItem.id && R.sentDate > Date.now())}
          keyExtractor={(n) => (`${n.id}`)}
          renderItem={({ item }) => (
            <View>
              <ReminderItem
                id={item.id}
                type={item.type}
                sentDate={item.sentDate}
              />
            </View>
          )}
        />
      </View>
    </View >
  );
};


FridgeItemScreen.navigationOptions = (navData: NavigationParams) => ({
  headerTitle: 'Fridge Item',
  headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-clock"
        onPress={() => {
          navData.navigation.navigate('AddReminder', { item: navData.navigation.getParam('item') });
        }}
      />
    </HeaderButtons>
  ),
});

export default FridgeItemScreen;
