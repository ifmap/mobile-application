import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import HeaderButton from '../../../components/ui/HeaderButton/HeaderButton';
import FridgeItem from '../../../components/fridgescreen/FridgeItem/FridgeItem';
import styles from './fridgeScreenStyle';
import { RootState } from '../../../App';
import { ActivityIndicator } from 'react-native';

/** Renders the page with all the fridge options */
const FridgeScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const storedFridgeItems = useSelector((state: RootState) => state.fridge.fridgeItems);
  const selectedGroup = useSelector((state: RootState) => state.fridgeGroups.fridgeGroups).find(G => G.id === navigation.getParam('id')) ?? {id: -1, name: "Uncategorized" };
  const fridgeItems = storedFridgeItems.filter(I => selectedGroup.id !== -1 ? I.fridgeGroupId === selectedGroup.id : !I.fridgeGroupId || I.fridgeGroupId === -1);
  const loadState = useSelector((state: RootState) => state.fridge.loading);
  
  if (!fridgeItems || fridgeItems.length == 0) { 
    return (
      <View style={styles.content}>
        <Text style={styles.title}>Category: {selectedGroup.name}</Text>
        <Text style={styles.emptyMessage}>This category is empty.</Text>
      </View>
    );
  }
  
  return (
    <View style={styles.content}>
      { loadState > 0 &&
        <View>
          <ActivityIndicator style={styles.icon} size="large" />
        </View>
      }
      <Text style={styles.title}>Category: {selectedGroup.name}</Text>
      <FlatList
        data={fridgeItems}
        keyExtractor={(n) => (`${n.id}`)}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('FridgeItem', { item })}
          >
            <FridgeItem
              id={item.id}
              name={item.name}
              type={item.type}
              date={item.date}
              expiryDate={item.expiryDate}
              status={item.status}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

FridgeScreen.navigationOptions = (navData: NavigationParams) => ({
  headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Add item"
        iconName="ios-egg"
        onPress={() => {
          navData.navigation.navigate('AddFridgeItem');
        }}
      />
    </HeaderButtons>
  ),
});

export default FridgeScreen;
