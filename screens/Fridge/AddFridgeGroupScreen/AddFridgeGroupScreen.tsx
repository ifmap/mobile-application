import React, { useState } from 'react';
import {
  View, Button, TextInput, Text
} from 'react-native';
import { useDispatch } from 'react-redux';
import { NavigationParams } from 'react-navigation';

import styles from './addFridgeGroupScreenStyle';
import { addFridgeGroupAsync } from '../../../store/fridgeGroup/actions';

/** Renders the sign up screen. */
const AddFridgeGroupScreen: React.FC<NavigationParams> = ({ navigation }) => {
  const dispatch = useDispatch();

  const [name, setName] = useState<string | undefined>();
  
  const addPress = (): void => {
    if (name) {
      dispatch(addFridgeGroupAsync({ name }));
      navigation.navigate('FridgeGroup');
    }
  }

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <TextInput
          placeholder="Name"
          onChangeText={(value) => setName(value)}
        />
        {
          !name &&
          <Text style={styles.error}>Please enter a valid name.</Text>
        }
        <Button
          title="Create the item group"
          onPress={addPress}
          disabled={!name}
        />
      </View>
    </View>

  );
};

export default AddFridgeGroupScreen;
