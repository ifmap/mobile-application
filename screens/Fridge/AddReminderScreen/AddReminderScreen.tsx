import React, { useState } from 'react';
import {
  View, Button, Text, Platform
} from 'react-native';
import { useDispatch } from 'react-redux';
import { NavigationParams } from 'react-navigation';
import DateTimePicker from '@react-native-community/datetimepicker';

import styles from './addReminderScreenStyle';
import { addReminderAsync } from '../../../store/reminder/actions';

const AddReminderScreen: React.FC<NavigationParams> = ({ navigation }) => {
  const dispatch = useDispatch();
  const item = navigation.getParam('item');
  const [date, setDate] = useState(new Date(Date.now() - 100));
  const [mode, setMode] = useState<"date" | "time" | "datetime" | undefined>('date');
  const [show, setShow] = useState(false);

  const onChange = (event: Event, selectedDate: Date | undefined) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode: "date" | "time" | "datetime" | undefined) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const addPress = (): void => {
    if (date) {
      dispatch(
        addReminderAsync({
          fridgeItemId: item.id,
          title: `Your reminder for ${item.type}: ${item.name} has expired.`,
          severity: 4,
          type: "Reminder",
          isDeletable: true,
          sentDate: date.valueOf(),
        })
      );
      navigation.navigate('FridgeItem', { item });
    }
  }

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <View>
          <Button onPress={showDatepicker} title={"Date: " + date.toDateString()} />
        </View>
        <View>
          <Button onPress={showTimepicker} title={"Time: " + date.toLocaleTimeString()} />
        </View>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={mode}
            onChange={onChange}
          />
        )}
        {
          (date.valueOf() < Date.now()) &&
          <Text style={styles.error}>Please enter a date in the future.</Text>
        }

        <Button // modifier pour faire un bouton de redirection
          title={"Add the reminder to: " + item.name}
          onPress={addPress}
          disabled={date.valueOf() < Date.now()}
        />
      </View>
    </View>

  );
};

export default AddReminderScreen;
