import { StyleSheet } from 'react-native';

import Colors from '../../utils/colors';

export default StyleSheet.create({
    cameraContainer: {
        alignItems: 'center',
        backgroundColor: Colors.black,
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        position: 'absolute',
        width: '100%',
    },
    container: {
        flex: 1,
    },
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    globalContainer: {
        alignItems: 'center',
        flex: 1,
    },
    icon: {
        alignItems: 'center',
        backgroundColor: Colors.accent,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        elevation: 4,
        height: 35,
        justifyContent: 'center',
        left: 0,
        opacity: 0.8,
        overflow: 'hidden',
        position: 'absolute',
        top: '12%',
        width: 40,
    },
});