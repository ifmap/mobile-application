import { Alert } from 'react-native';
import * as firebase from 'firebase';
import "firebase/auth";
  
export const signUpUser = async (email: string, password: string): Promise<void>  => {
    try {
        await firebase.auth().createUserWithEmailAndPassword(email, password);
    } catch (error) {
        Alert.alert('Error',
            'Unable to create an account using this email and password.',
            [ { text: 'OK' }],
            { cancelable: false },
        );
    }
}
  