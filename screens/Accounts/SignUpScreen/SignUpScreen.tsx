import React, { useState } from 'react';
import {
  View, Button, TextInput, Alert, Text
} from 'react-native';
import { NavigationParams } from 'react-navigation';

import styles from './signUpScreenStyle';
import { signUpUser } from './signUpScreen.util';
import colors from '../../../utils/colors';

/** Renders the sign up screen. */
const SignUpScreen: React.FC<NavigationParams> = ({ navigation }) => {

  const [email, setEmail] = useState<string | undefined>();
  const [password, setPassword] = useState<string | undefined>();
  const [confirmPassword, setConfirmPassword] = useState<string | undefined>();

  let hasEmailErrors = false;
  let hasPasswordErrors = false;
  let hasSamePasswordErrors = false;
  const emailRegexp = /[\w-]+@([\w-]+\.)+[\w-]+/;
  const passwordRegexp = /^([a-zA-Z0-9@*#]{8,15})$/;
  
  if (email && !email.match(emailRegexp)) { 
    hasEmailErrors = true;
  }
  
  if (password && !password.match(passwordRegexp)) { 
    hasPasswordErrors = true;
  }
  
  if ( password && password != confirmPassword) { 
    hasSamePasswordErrors = true;
  }
  
  const signUpPress = (): void => {
    if (email != undefined && password != undefined) {
      signUpUser(email, password)
    } else {
      Alert.alert('Error',
        'Please provide an email and a password.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    }
  }

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <TextInput
          placeholder="Email"
          onChangeText={(value) => setEmail(value)}
        />
        {
          hasEmailErrors &&
          <Text style={styles.error}>Please enter a valid email.</Text>
        }
        <TextInput
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={(value) => setPassword(value)}
        />
        {
          hasPasswordErrors &&
          <Text style={styles.error}>Password must be 8 to 15 characters long.</Text>
        }
        <TextInput
          placeholder="Confirm password"
          secureTextEntry={true}
          onChangeText={(value) => setConfirmPassword(value)}
        />
        {
          hasSamePasswordErrors &&
          <Text style={styles.error}>Passwords do not match.</Text>
        }
        <Button
          title="Sign Up"
          disabled={!email || !password || hasPasswordErrors  || hasSamePasswordErrors || hasEmailErrors}
          onPress={() => signUpPress()}
        />
        <Button // modifier pour faire un bouton de redirection
          title="Sign in with an existing account"
          onPress={() => { navigation.navigate('SignIn'); }}
        />
        <Button
          title="Continue as guest"
          color={colors.darkGray}
          onPress={() => {
            navigation.navigate('Fruit Scanner');
          }}
        />
      </View>
    </View>

  );
};

export default SignUpScreen;
