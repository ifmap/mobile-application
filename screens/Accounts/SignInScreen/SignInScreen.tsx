import React, { useState } from 'react';
import {
  View, Button, TextInput, Alert, Text
} from 'react-native';
import { logInUser } from './signInScreen.util'
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../../../components/ui/HeaderButton/HeaderButton';
import styles from './signInScreenStyle';
import { storeGuest } from '../../../utils/helpers';
import colors from '../../../utils/colors';

/** Renders the sign in screen. */
const SignInScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const [email, setEmail] = useState<string | undefined>();
  const [password, setPassword] = useState<string | undefined>();
  
  let hasEmailErrors = false;
  let hasPasswordErrors = false;
  const emailRegexp = /[\w-]+@([\w-]+\.)+[\w-]+/;
  const passwordRegexp = /^([a-zA-Z0-9@*#]{8,15})$/;

  if (email && !email.match(emailRegexp)) {
    hasEmailErrors = true;
  }

  if (password && !password.match(passwordRegexp)) {
    hasPasswordErrors = true;
  }

  const signInPress = (): void => {
    if (email != undefined && password != undefined) {
      logInUser(email, password)
    } else {
      Alert.alert('Error',
        'Please provide an email and a password.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    }
  }
  
  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <TextInput
          placeholder="Email"
          onChangeText={(value) => setEmail(value)}
        />
        {
          hasEmailErrors &&
          <Text style={styles.error}>Please enter a valid email.</Text>
        }
        <TextInput
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={(value) => setPassword(value)}
        />
        {
          hasPasswordErrors &&
          <Text style={styles.error}>Password must be 8 to 15 characters long.</Text>
        }
        <Button
          title="Sign In"
          disabled={!email || !password || hasPasswordErrors || hasEmailErrors}
          onPress={() => signInPress()}
        />
        <Button // modifier pour faire un bouton de redirection
          title="Create an account"
          onPress={() => { navigation.navigate('SignUp'); }}
        />
        <Button
          title="Continue as guest"
          color={colors.darkGray}
          onPress={() => {
            storeGuest('true');
            navigation.navigate('Fruit Scanner');
          }}
        />
      </View>
    </View>

  );
};

SignInScreen.navigationOptions = (navData: NavigationParams) => ({
  headerTitle: "Sign in",
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
})

export default SignInScreen;
