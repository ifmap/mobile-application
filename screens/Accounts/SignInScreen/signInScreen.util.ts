import { Alert } from 'react-native';
import * as firebase from 'firebase';
import "firebase/auth";
  
export const logInUser = async (email: string, password: string): Promise<void> => {
    try {
        await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
        Alert.alert('Error',
            'Invalid credentials',
            [ { text: 'OK' }],
            { cancelable: false },
        );
    }
}