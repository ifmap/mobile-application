import React from 'react';
import {
  View, Button, Text
} from 'react-native';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import * as firebase from 'firebase';
import "firebase/auth";

import HeaderButton from '../../../components/ui/HeaderButton/HeaderButton';
import styles from './accountScreenStyle';
import UpdateEmail from '../../../components/accountscreen/UpdateEmail';
import UpdatePassword from '../../../components/accountscreen/UpdatePassword';
import UpdateName from '../../../components/accountscreen/UpdateName';
import colors from '../../../utils/colors';
import DeleteAccount from '../../../components/accountscreen/DeleteAccount';

/** Renders the account screen. */
const AccountScreen: NavigationStackScreenComponent = () => {
  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <Text style={styles.title}>
          Logged in as {
            firebase.auth().currentUser?.displayName || firebase.auth().currentUser?.email
          }
        </Text>
        <UpdateName />
        <UpdateEmail />
        <UpdatePassword />
        <DeleteAccount />
        <Button
          title="Sign Out"
          color={colors.darkGray}
          onPress={() => {
            firebase.auth().currentUser && firebase.auth().signOut();
          }}
        />
      </View>
    </View>
  );
};

AccountScreen.navigationOptions = (navData: NavigationParams) => ({
  headerTitle: "Account",
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
})

export default AccountScreen;
