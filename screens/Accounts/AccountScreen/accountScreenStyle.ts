import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors'

export default StyleSheet.create({
    buttons: {
        height: 350,
        justifyContent: 'space-between',
        marginTop: 50,
        width: 250,
    },
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between',
    },
    error: {
        color: Colors.error,
    },
    title: {
        color: Colors.primary,
        fontSize: 20,
        marginBottom: 25,
    }
});
