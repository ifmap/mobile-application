import { StyleSheet, Dimensions } from 'react-native';

import Colors from '../../utils/colors';

export default StyleSheet.create({
    action: {
         width: 100,
    },
    actionContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        width: 250,
    },
    content: {
        alignItems: 'center',
        flex: 1,
        marginTop: 25,
    },
    image: {
        height: '100%',
        width: '100%',
    },
    imagePreview: {
        alignItems: 'center',
        borderColor: Colors.gray,
        borderWidth: 1,
        height: Dimensions.get('window').width * 0.60,
        justifyContent: 'center',
        marginBottom: 10,
        width: Dimensions.get('window').width * 0.60,
    },
    text: {
        fontFamily: 'poppins-regular',
        fontSize: 18,
        textAlign: 'center',
    },
    textContainer: {
         marginVertical: 15,
    },
    title: {
        fontFamily: 'poppins-regular',
        fontSize: 21,
        textAlign: 'center',
    },
    titleContainer: {
        marginBottom: 20,
    },
});