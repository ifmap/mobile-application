import Colors from "../../utils/colors";

export interface ResultsProps {
    Name: string;
    Result: string;
    CertaintyFruit: string;
    CertaintyFresh: string;
}

export interface DetailsProps {
    title: string;
    accuracy: string;
    freshness: string;
    isSaveable: boolean;
    fruitColor: string;
    accuracyColor: string;
    freshnessColor: string;
}

export const getDetails = (results: ResultsProps): DetailsProps => {
    const {
        Name, Result, CertaintyFruit, CertaintyFresh,
    } = results;
    let accuracyColor = Colors.failedResult,
        freshnessColor = Colors.failedResult;

    switch ((Name || '').toLowerCase()) {
        case 'none':
            return {
                title: 'Your fruit was not recognized',
                accuracy: 'Make sure your fruit is within the frame',
                freshness: '',
                isSaveable: false,
                fruitColor: Colors.failedResult,
                accuracyColor,
                freshnessColor
            };
        case 'offline':
        case '':
            return {
                title: 'The server is currently offline',
                accuracy: 'Please try again later',
                freshness: '',
                isSaveable: false,
                fruitColor: Colors.failedResult,
                accuracyColor,
                freshnessColor
            };
        default:
            const isAn = ['a', 'o', 'i', 'u', 'y', 'e'].indexOf(Name.substring(0, 1)) > -1;
            let accuracy: string,
                freshness: string;

            // Handles display in freshness area (colors/text)
            if (Result.toLowerCase() === 'none') {
                accuracy = 'You may need to take a clearer picture.';
                freshness = 'Unable to the determine freshness level.';
            } else {
                accuracy = `Accuracy of prediction: ${CertaintyFresh}%`;
                freshness = `Freshness level : ${Result}`;
                accuracyColor = Colors.primary;
                if (Result.toLowerCase() === 'rotten') {
                    freshnessColor = Colors.rottenResult;
                } else {
                    freshnessColor = Colors.freshResult;
                }
            }

            return {
                title: `This fruit is ${isAn ? 'an' : 'a'} ${Name} (${CertaintyFruit}%)`,
                accuracy,
                freshness,
                isSaveable: true,
                fruitColor: Colors.primary,
                accuracyColor,
                freshnessColor
            };
    }
};