import React from 'react';
import {
  View,
  Text,
  Button,
  Image,
  Alert
} from 'react-native';
import { NavigationParams } from 'react-navigation';
import { useDispatch } from 'react-redux';
import * as firebase from 'firebase';
import "firebase/auth";

import { getDetails, ResultsProps } from './pictureDetailsScreen.util';
import { addFridgeItemAsync } from '../../store/fridge/actions';
import styles from './pictureDetailsScreenStyle';

/** Renders the result screen once the data was received */
const PictureDetailsScreen: React.FC<NavigationParams> = ({ navigation }) => {
  const dispatch = useDispatch();
  const auth = firebase.auth();
  const results: ResultsProps = navigation.getParam('results');
  const image: string = navigation.getParam('image');

  const {
    title, accuracy, freshness, isSaveable, fruitColor, accuracyColor, freshnessColor
  } = getDetails(results);

  const addToFridge = () => {
    if (auth.currentUser) {
      dispatch(addFridgeItemAsync({ name: results.Name, type: 'Scanned', status: results.Result, date: Date.now() }));
      navigation.navigate('FridgeGroup');
    } else {
      Alert.alert(
        'Login Required!',
        'Must be logged in to perform this operation.',
        [
          { text: 'OK' },
          { text: 'Login', onPress: () => { navigation.navigate('SignIn'); } },
          { text: 'Sign up', onPress: () => { navigation.navigate('SignUp'); } }
        ]
      );
    }
  }

  return (
    <View style={styles.content}>
      <View style={styles.titleContainer}>
        <Text style={{ ...styles.title, color: fruitColor }}>
          {title}
        </Text>
      </View>
      <View style={styles.imagePreview}>
        <Image
          style={styles.image}
          source={{ uri: image }}
        />
      </View>
      <View style={styles.textContainer}>
        <Text style={{ ...styles.text, color: freshnessColor }}>
          {freshness}
        </Text>
        <Text style={{ ...styles.text, color: accuracyColor }}>
          {accuracy}
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <View style={styles.action}>
          <Button title="Back" onPress={() => navigation.goBack()} />
        </View>
        <View style={styles.action}>
          <Button
            title="Save"
            disabled={!isSaveable}
            onPress={addToFridge}
          />
        </View>
      </View>
    </View>
  );
};

export default PictureDetailsScreen;
