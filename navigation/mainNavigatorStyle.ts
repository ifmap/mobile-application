import { StyleSheet } from 'react-native';

import Colors from '../utils/colors';

export default StyleSheet.create({
  drawer: {
    flex: 1,
    paddingTop: 20,
  },
  labelBody: {
    color: Colors.gray,
    fontFamily: 'poppins-regular',
    fontSize: 14,
  },
  labelContainer: {
    alignContent: 'center',
    alignItems: 'center',
    borderBottomColor: Colors.gray,
    borderBottomWidth: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
  },
  labelHeader: {
    fontFamily: 'poppins-regular',
    fontSize: 28,
  },
});
