import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import TabBarIcon from '../components/ui/TabBarIcon/TabBarIcon';
import SignInScreen from '../screens/Accounts/SignInScreen/SignInScreen';
import SignUpScreen from '../screens/Accounts/SignUpScreen/SignUpScreen';
import {
  defaultStackNavigationOptions, defaultDrawerOptions, MainStack, SettingsStack
} from './MainNavigator';

/** Renders the navigator with various screen stacks, also where the menu header is customized */

/** Login stack */
const AuthStack = createStackNavigator({
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      headerTitle: 'Welcome!',
    },
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      headerTitle: 'Sign up',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: (): JSX.Element => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-log-in' : 'md-log-in'}
      />
    ),
  },
});

/** The drawer with the stacks (menu) */
const GuestDrawer = createDrawerNavigator({
  'Fruit Scanner': MainStack,
  'Sign in': AuthStack,
  'App Settings': SettingsStack,
}, defaultDrawerOptions);

const App = createSwitchNavigator({
  App: GuestDrawer,
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
});

export default createAppContainer(App);
