import React from 'react';
import {
  Platform,
  View,
  Text,
  SafeAreaView
} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator, NavigationStackOptions } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems } from 'react-navigation-drawer';
import { DrawerNavigatorItemsProps, NavigationDrawerOptions } from 'react-navigation-drawer/lib/typescript/src/types';

import TabBarIcon from '../components/ui/TabBarIcon/TabBarIcon';
import MainScreen from '../screens/MainScreen/MainScreen';
import PictureDetailsScreen from '../screens/PictureDetailsScreen/PictureDetailsScreen';
import AccountScreen from '../screens/Accounts/AccountScreen/AccountScreen';
import SettingsScreen from '../screens/SettingsScreen/SettingsScreen';
import FridgeGroupsScreen from '../screens/Fridge/FridgeGroupsScreen/FridgeGroupsScreen';
import FridgeScreen from '../screens/Fridge/FridgeScreen/FridgeScreen';
import FridgeItemScreen from '../screens/Fridge/FridgeItemScreen/FridgeItemScreen';
import AddFridgeGroupScreen from '../screens/Fridge/AddFridgeGroupScreen/AddFridgeGroupScreen';
import AddFridgeItemScreen from '../screens/Fridge/AddFridgeItemScreen/AddFridgeItemScreen';
import AddReminderScreen from '../screens/Fridge/AddReminderScreen/AddReminderScreen';
import Colors from '../utils/colors';
import styles from './mainNavigatorStyle';

/** Renders the navigator with various screen stacks, also where the menu header is customized */
export const customDrawerNavigation: React.FC<DrawerNavigatorItemsProps> = (props) => (
  <View style={styles.drawer}>
    <SafeAreaView>
      <View style={styles.labelContainer}>
        <Text style={styles.labelHeader}>
          IFMAP - LRIMa
        </Text>
        <Text style={styles.labelBody}>
          Jihene Rezgui, Thomas Trépanier, David Génois, Hervé Claden and Amasten Ameziani
        </Text>
      </View>
      <DrawerNavigatorItems {...props} />
    </SafeAreaView>
  </View>
);

/** Default navigation options */
export const defaultStackNavigationOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : '',
  },
  headerTitleStyle: {
    fontFamily: 'open-sans-bold',
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans',
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
  headerTitle: 'Default Title',
} as NavigationStackOptions;

export const defaultDrawerOptions = {
  defaultNavigationOptions: defaultStackNavigationOptions,
  drawerType: "slide",
  minSwipeDistance: 500,
  title: 'test',
  contentComponent: customDrawerNavigation,
} as NavigationDrawerOptions

/** Account stack */
const AccountStack = createStackNavigator({
  Account: {
    screen: AccountScreen,
    navigationOptions: {
      headerTitle: 'My account',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: (): JSX.Element => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-log-in' : 'md-log-in'}
      />
    ),
  },
});

/** Mainscreen Stack */
export const MainStack = createStackNavigator({
  Main: {
    screen: MainScreen,
    navigationOptions: {
      header: null,
    },
  },
  PictureDetails: {
    screen: PictureDetailsScreen,
    navigationOptions: {
      headerTitle: 'Analysis Results',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: (): JSX.Element => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-qr-scanner' : 'md-qr-scanner'}
      />
    ),
  },
});

/** Setting Stack */
export const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      headerTitle: 'Settings',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: (): JSX.Element => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-settings' : 'md-settings'}
      />
    ),
  },
});

/** Virtual Fridge Stack */
const FridgeStack = createStackNavigator({
  FridgeGroup: {
    screen: FridgeGroupsScreen,
    navigationOptions: {
      headerTitle: 'Virtual Fridge',
    },
  },
  Fridge: {
    screen: FridgeScreen,
    navigationOptions: {
      headerTitle: 'Virtual Fridge Category',
    },
  },
  FridgeItem: {
    screen: FridgeItemScreen,
    navigationOptions: {
      headerTitle: 'Virtual Fridge Item',
    },
  },
  AddFridgeGroup: {
    screen: AddFridgeGroupScreen,
    navigationOptions: {
      headerTitle: 'New Fridge Category',
    },
  },
  AddFridgeItem: {
    screen: AddFridgeItemScreen,
    navigationOptions: {
      headerTitle: 'New Fridge Item',
    },
  },
  AddReminder: {
    screen: AddReminderScreen,
    navigationOptions: {
      headerTitle: 'New Item Reminder',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-basket' : 'md-basket'}
      />
    ),
  },
});

/** The drawer with the stacks (menu) */
const MainDrawer = createDrawerNavigator({
  'Fruit Scanner': MainStack,
  'Virtual Fridge': FridgeStack,
  'Account':  AccountStack,
  'App Settings': SettingsStack,
}, defaultDrawerOptions);

const App = createSwitchNavigator({
  App: MainDrawer,
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
});

export default createAppContainer(App);
