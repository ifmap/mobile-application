import React, { useState } from 'react';
import {
  View, Button, TextInput, Alert, Text
} from 'react-native';
import { NavigationParams } from 'react-navigation';
import * as firebase from 'firebase';
import "firebase/auth";

import styles from './updateComponentsStyle'

/** Renders the update name component. */
const UpdateName: React.FC<NavigationParams> = () => {
  const fireAuth = firebase.auth();
  const user = fireAuth.currentUser;
  const [name, setName] = useState<string | undefined>();

  // Validation
  let hasNameError = false;
  const nameRegexp = /[a-z0-9_-]{3,16}/;

  if (name && !name.match(nameRegexp)) {
    hasNameError = true;
  }

  // Update data
  const onNameUpdate = async (): Promise<void> => {
    if (!user || !name) {
      return;
    }
    try {
      await user.updateProfile({ displayName: name });
      Alert.alert('Success',
        'Name updated.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    } catch {
      Alert.alert('Error',
        'Invalid name.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    }
  };

  return (
    <View style={styles.content}>
      <Text>Update Display Name:</Text>
      <TextInput
        placeholder={user && user.displayName || 'Display Name'}
        onChangeText={(value) => setName(value)}
      />
      {
        hasNameError &&
        <Text style={styles.error}>Display name must be 4 to 16 characters long with no space or number.</Text>
      }
      <Button
        title="Save"
        disabled={!name || hasNameError}
        onPress={() => onNameUpdate()}
      />
    </View>
  );
};

export default UpdateName;
