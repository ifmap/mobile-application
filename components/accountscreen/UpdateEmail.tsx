import React, { useState } from 'react';
import {
  View, Button, TextInput, Alert, Text
} from 'react-native';
import { NavigationParams } from 'react-navigation';
import * as firebase from 'firebase';
import "firebase/auth";

import styles from './updateComponentsStyle'

/** Renders the update email component. */
const UpdateEmail: React.FC<NavigationParams> = () => {
  const fireAuth = firebase.auth();
  const user = fireAuth.currentUser;
  const [email, setEmail] = useState<string | undefined>();

  // Validation
  let hasEmailErrors = false;
  const emailRegexp = /[\w-]+@([\w-]+\.)+[\w-]+/;

  if (email && !email.match(emailRegexp)) {
    hasEmailErrors = true;
  }

  // Update data
  const onEmailUpdate = async (): Promise<void> => {
    if (!user || !email) {
      return;
    }
    try {
      await user.updateEmail(email);
      Alert.alert('Success',
        'Email updated.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    } catch {
      Alert.alert('Error',
        'Email is invalid or the current session is too old.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    }
  };

  return (
    <View style={styles.content}>
      <Text>Update Email:</Text>
      <TextInput
        placeholder={user && user.email || 'Email'}
        onChangeText={(value) => setEmail(value)}
      />
      {
        hasEmailErrors &&
        <Text style={styles.error}>Please enter a valid email.</Text>
      }
      <Button
        title="Save"
        disabled={!email || hasEmailErrors}
        onPress={() => onEmailUpdate()}
      />
    </View>
  );
};

export default UpdateEmail;
