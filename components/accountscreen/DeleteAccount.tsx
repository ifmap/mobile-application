import React, { useState } from 'react';
import { View, Button, TextInput, Alert } from 'react-native';
import { NavigationParams } from 'react-navigation';
import * as firebase from 'firebase';
import "firebase/auth";

import styles from './updateComponentsStyle'
import colors from '../../utils/colors';

/** Renders the update email component. */
const DeleteAccount: React.FC<NavigationParams> = () => {
  const fireAuth = firebase.auth();
  const user = fireAuth.currentUser;
  const [confirm, setConfirm] = useState<string | undefined>();


  // Update data
  const onAccountDelete = async (): Promise<void> => {
    try {
      if (user?.email !== confirm) {
        Alert.alert('Need Confirmation',
          'Please enter your email address above to confirm you want to delete your account permanently.',
          [
            { text: 'OK' },
          ]
        );
        return;
      }
      await user?.delete();
      Alert.alert('Success',
        'Account permanently deleted.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    } catch {
      Alert.alert('Error',
        'Sensitive operation, please sign out and sign back in before attempting to delete your account.',
        [
          { text: 'OK' },
        ]
      );
    }
  };

  return (
    <View style={styles.content}>
      <TextInput placeholder="Enter email to confirm deletion" onChangeText={(value) => setConfirm(value)}></TextInput>
      <Button
        title="Permanently delete my account"
        color={colors.error}
        onPress={() => onAccountDelete()}
      />
    </View>
  );
};

export default DeleteAccount;
