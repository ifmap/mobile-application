import { StyleSheet } from 'react-native';
import colors from '../../utils/colors';


export default StyleSheet.create({
    buttons: {
        marginBottom: 10,
        marginTop: 10,
    },
    content: {
        marginBottom: 20
    },
    error: {
        color: colors.error,
    },
});
