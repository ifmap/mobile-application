import React, { useState } from 'react';
import {
  View, Button, TextInput, Alert, Text
} from 'react-native';
import { NavigationParams } from 'react-navigation';
import * as firebase from 'firebase';
import "firebase/auth";

import styles from './updateComponentsStyle'

/** Renders the update password component. */
const UpdatePassword: React.FC<NavigationParams> = () => {
  const fireAuth = firebase.auth();
  const user = fireAuth.currentUser;
  const [password, setPassword] = useState<string | undefined>();
  const [confirmPassword, setConfirmPassword] = useState<string | undefined>();

  // Validation
  let hasPasswordErrors = false;
  let hasSamePasswordErrors = false;
  const passwordRegexp = /^([a-zA-Z0-9@*#]{8,15})$/;

  if (password && !password.match(passwordRegexp)) {
    hasPasswordErrors = true;
  }

  if (password && password != confirmPassword) {
    hasSamePasswordErrors = true;
  }

  // Update data

  const onPasswordUpdate = async (): Promise<void> => {
    if (!user || !password) {
      return;
    }
    try {
      await user.updatePassword(password);
      Alert.alert('Success',
        'Password updated.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
    } catch {
      Alert.alert('Error',
        'Password is invalid or the current session is too old.',
        [ { text: 'OK' }],
        { cancelable: false },
      );
      }
    

  };

  return (
    <View style={styles.content}>
      <Text>Update Password:</Text>
      <TextInput
        placeholder="Password"
        secureTextEntry={true}
        onChangeText={(value) => setPassword(value)}
      />
      {
        hasPasswordErrors &&
        <Text style={styles.error}>Password must be 8 to 15 characters long.</Text>
      }
      <TextInput
        placeholder="Confirm password"
        secureTextEntry={true}
        onChangeText={(value) => setConfirmPassword(value)}
      />
      {
        hasSamePasswordErrors &&
        <Text style={styles.error}>Passwords do not match.</Text>
      }
      <Button
        title="Save"
        disabled={!password || hasPasswordErrors || hasSamePasswordErrors}
        onPress={() => onPasswordUpdate()}
      />
    </View>
  );
};

export default UpdatePassword;
