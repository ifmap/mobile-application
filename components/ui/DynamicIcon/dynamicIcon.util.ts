import {
    MaterialCommunityIcons,
    MaterialIcons,
    Feather,
    EvilIcons,
} from '@expo/vector-icons';
import { Icon } from '@expo/vector-icons/build/createIconSet';

export const getIconName = (type: string): Icon<string, string> | null => {
    switch (type) {
        case 'MaterialCommunityIcons':
            return MaterialCommunityIcons;
        case 'MaterialIcons':
            return MaterialIcons;
        case 'EvilIcons':
            return EvilIcons;
        case 'Feather':
            return Feather;
        default:
            return null;
    }
};