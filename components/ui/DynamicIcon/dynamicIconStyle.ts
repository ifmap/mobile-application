import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    txtIcon: {
        color: Colors.accent,
        fontSize: 40,
        marginBottom: 10,
    },
});
