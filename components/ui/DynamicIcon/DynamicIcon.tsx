import React from 'react';
import { TouchableOpacity, ViewStyle, TextStyle } from 'react-native';

import { getIconName } from './dynamicIcon.util';
import styles from './dynamicIconStyle';

interface DynamicIconProps {
  touchableStyle?: ViewStyle;
  onPressHandler: () => void;
  name: string;
  type: string;
  size?: number;
  iconStyle?: TextStyle;
}

/** Custom Icon Handler to render an icon of various types without the need to import many items */
const DynamicIcon: React.FC<DynamicIconProps> = ({
  touchableStyle,
  onPressHandler,
  name,
  type,
  size,
  iconStyle,
  children
}) => {
  const CustomIcon = getIconName(type);

  return (
    <TouchableOpacity style={touchableStyle} onPress={() => onPressHandler()}>
      {CustomIcon && <CustomIcon name={name} type={type} size={size || 40} style={iconStyle || styles.txtIcon} />}
      {children}
    </TouchableOpacity>
  );
};

export default DynamicIcon;
