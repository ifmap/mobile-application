import React from 'react';
import { Platform } from 'react-native';
import { HeaderButton, HeaderButtonProps } from 'react-navigation-header-buttons';
import { Ionicons } from '@expo/vector-icons';

import Colors from '../../../utils/colors';

/** Represents the header button */
const CustomHeaderButton: React.FC<HeaderButtonProps> = (props) => (
  <HeaderButton
    {...props}
    IconComponent={Ionicons}
    iconSize={23}
    color={Platform.OS === 'android' ? 'white' : Colors.primary}
  />
);

export default CustomHeaderButton;
