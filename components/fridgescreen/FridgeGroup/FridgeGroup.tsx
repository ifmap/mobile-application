import React from 'react';
import {
  Text,
  Alert,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../../App';
import Icon from '../../ui/DynamicIcon/DynamicIcon';
import { FridgeGroupType } from '../../../utils/types';
import styles from './fridgeGroupStyle';
import { removeFridgeGroupOnlyAsync, removeFridgeGroupAndItemsAsync } from '../../../store/fridgeGroup/actions';

/** TODO: STYLING + THUMBNAIL?  */
const FridgeGroup: React.FC<FridgeGroupType> = ({
  id, name
}) => {
  const dispatch = useDispatch();
  const storedFridgeItems = useSelector((state: RootState) => state.fridge.fridgeItems);
  const selectedGroup = useSelector((state: RootState) => state.fridgeGroups.fridgeGroups).find(G => G.id === id) ?? { id: -1, name: "Uncategorized" };
  const fridgeItems = storedFridgeItems.filter(I => selectedGroup.id !== -1 ? I.fridgeGroupId === selectedGroup.id : !I.fridgeGroupId || I.fridgeGroupId === -1);

  const confirmDelete = () => {
    const alertOptions = [
      { text: 'CANCEL' },
      { text: 'YES, DELETE GROUP ONLY', onPress: () => id && dispatch(removeFridgeGroupOnlyAsync(id)) },
    ];
    if (fridgeItems.length > 0) {
      alertOptions.push({ text: 'YES, DELETE GROUP AND ITEMS', onPress: () => id && dispatch(removeFridgeGroupAndItemsAsync(id)) });
    }
    Alert.alert(
      'Need Confirmation',
      `Are you sure you want to delete the following group: ${name}" ?`,
      alertOptions
    );
  }

  // Treat uncategorized differently
  if (id === -1) {
    if (storedFridgeItems.length === 0) {
      return (
        <View style={styles.fridgeGroup}>
          <Text style={styles.emptyMessage}>
            Your virtual fridge is empty. Start off by adding a new category or an item!
          </Text>
        </View>
      );
    }
    if (fridgeItems.length === 0) {
      return null;
    }
    return (
      <View style={styles.fridgeGroup}>
        <Text style={styles.text}>{name} ({fridgeItems.length})</Text>
      </View>
    );
  }

  return (
    <View style={styles.fridgeGroup}>
      <View>
        <Text style={styles.text}>{name} ({fridgeItems.length})</Text>
      </View>
      {id != null &&
        <View>
          <Icon
            name="trash"
            type="EvilIcons"
            iconStyle={styles.deleteIcon}
            onPressHandler={confirmDelete}
          />
        </View>
      }
    </View>
  );
};

export default FridgeGroup;
