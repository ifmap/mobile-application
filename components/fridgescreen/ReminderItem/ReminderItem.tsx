import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';

import Icon from '../../ui/DynamicIcon/DynamicIcon';
import styles from './reminderItemStyle';
import { removeReminderAsync } from '../../../store/reminder/actions';
import { ReminderItemType } from '../../../utils/types';

/** TODO: STYLING   */
const ReminderItem: React.FC<ReminderItemType> = ({
  id, type, sentDate,
}) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.reminder}>
      <View>
        {sentDate && <Text style={styles.text}>{type}: {(new Date(sentDate)).toLocaleString()}</Text>}
      </View>
      {id != null &&
        <View>
          <Icon
            name="trash"
            type="EvilIcons"
            iconStyle={styles.deleteIcon}
            onPressHandler={() => dispatch(removeReminderAsync(id))}
          />
        </View>
      }
    </View>
  );
};

export default ReminderItem;
