import { StyleSheet } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    deleteIcon: {
        color: Colors.deleteRed,
        fontSize: 40,
    },
    fridgeItem: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        paddingLeft: 8,
    },
    text: {
        color: Colors.freshResult,
        fontSize: 23
    }
});