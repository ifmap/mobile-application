import React from 'react';
import {
  Text,
  Alert,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';

import Icon from '../../ui/DynamicIcon/DynamicIcon';
import { FridgeItemType } from '../../../utils/types';
import styles from './fridgeItemStyle';
import { removeFridgeItemAsync } from '../../../store/fridge/actions';

/** TODO: STYLING + THUMBNAIL?  */
const FridgeItem: React.FC<FridgeItemType> = ({ id, type, name }) => {
  const dispatch = useDispatch();

  const confirmDelete = () => {
    Alert.alert('Need Confirmation',
      `Are you sure you want to delete "${type}: ${name}" ?`,
      [
        { text: 'YES', onPress: () => id && dispatch(removeFridgeItemAsync(id)) },
        { text: 'CANCEL' },
      ]
    );
  }

  return (
    <View style={styles.fridgeItem}>
      <View>
        <Text style={styles.text}>{type}: {name}</Text>
      </View>
      {id != null &&
        <View>
          <Icon
            name="trash"
            type="EvilIcons"
            iconStyle={styles.deleteIcon}
            onPressHandler={confirmDelete}
          />
        </View>
      }
    </View>
  );
};

export default FridgeItem;
