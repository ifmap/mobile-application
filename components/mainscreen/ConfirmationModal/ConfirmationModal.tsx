import React, { useState } from 'react';
import {
  Alert, View, Button, Text, Image, Modal, ActivityIndicator
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';

import Colors from '../../../utils/colors';
import { getFormData } from './confirmationModal.util';
import styles from './confirmationModalStyle';

interface ConfirmationModalProps {
  pickedImage: string;
  isModalVisible: boolean;
  setIsModalVisible: (b: boolean) => void;
  navigation: NavigationStackProp
}

/** Renders the modal on screen to confirm the picture being sent */
const ConfirmationModal: React.FC<ConfirmationModalProps> = ({
  pickedImage,
  isModalVisible,
  setIsModalVisible,
  navigation,
}) => {
  const [isUploading, setIsUploading] = useState<boolean>(false);

  /** Sends an image to the server for analysis
   * @param uri - link to the currently stored picture
   */
  const uploadImageAsync = async (uri: string): Promise<void> => {
    try {
      // Using local ip for testing while the server is down
      const response = await fetch('http://206.167.36.206/api/image', {
        method: 'POST',
        headers: { 'content-type': `multipart/form-data` },
        body: getFormData(uri),
      });

      const data = await response.json();

      setIsModalVisible(!isModalVisible);

      navigation.navigate('PictureDetails', {
        image: pickedImage,
        results: data,
      });
    } catch(e) {
      Alert.alert(
        'Error',
        'Error fetching response from the server. \n Make sure you are using a photo under 5mb and make sure you are connected to the internet.',
        [{ text: 'OK' }]
      );
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent
      visible={isModalVisible}
    >
      <View style={styles.modal}>
        <View style={styles.modalContent}>
          <View>
            <View style={styles.imagePreview}>
              {!pickedImage ? (
                <View style={styles.imageEmpty} />
              ) : (
                <Image style={styles.image} source={{ uri: pickedImage }} />
              )}
            </View>
            <Text style={styles.modalText}>
              {!isUploading ? 'Do you want to send this picture?' : 'Analyzing picture...'}
            </Text>
            {!isUploading
              ? (
                <View style={styles.modalActions}>
                  <View style={styles.modalAction}>
                    <Button
                      title="Back"
                      color={Colors.primary}
                      onPress={() => setIsModalVisible(!isModalVisible)}
                    />
                  </View>
                  <View style={styles.modalAction}>
                    <Button
                      title="Send"
                      color={Colors.primary}
                      onPress={async () => {
                        setIsUploading(true);
                        await uploadImageAsync(pickedImage);
                      }}
                    />
                  </View>
                </View>
              )
              : <ActivityIndicator size="large" />}
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default ConfirmationModal;
