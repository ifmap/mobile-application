import { StyleSheet, Dimensions } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
	image: {
		height: '100%',
		width: '100%',
	},
	imageEmpty: {
		backgroundColor: Colors.transparent,
		height: '100%',
		width: '100%',
	},
	imagePreview: {
		alignItems: 'center',
		borderColor: Colors.gray,
		borderWidth: 1,
		height: Dimensions.get('window').width * 0.60,
		justifyContent: 'center',
		marginBottom: 10,
		width: Dimensions.get('window').width * 0.60,
	},
	modal: {
		alignItems: 'center',
		top: Dimensions.get('window').height * 0.15,
	},
	modalAction: {
		width: 100,
	},
	modalActions: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 15,
		paddingHorizontal: 15,
	},
	modalContent: {
		alignItems: 'center',
		backgroundColor: Colors.white,
		borderRadius: 15,
		height: Dimensions.get('window').height * 0.60,
		justifyContent: 'center',
		width: Dimensions.get('window').width * 0.90,
	},
	modalText: {
		fontSize: 15,
		marginVertical: 15,
		textAlign: 'center',
	},
});