import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Button,
  Modal,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';

import { getIconStyle, fetchNotifStatus, storeGlobalDelete } from './notificationItem.util';
import Colors from '../../../../utils/colors';
import styles from './notificationItemStyle';
import { removeReminderAsync } from '../../../../store/reminder/actions';

interface NotificationItemProps {
  id?: number;
  type: string;
  title: string;
  description: string;
  isDeletable: boolean
}

const NotificationItem: React.FC<NotificationItemProps> = ({
  id, type, title, description, isDeletable
}) => {
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [isNotifVisible, setIsNotifVisible] = useState<boolean | undefined>();

  const [icon, color] = getIconStyle(type);

  const deleteNotification = async (): Promise<void> => {
    setIsModalVisible(false);
    setIsNotifVisible(false); 
    if (isDeletable && id != null) {
      if (type === "Reminder") { 
        dispatch(removeReminderAsync(id));
      } else { 
        await storeGlobalDelete(id);
      }
    }
  };

  //  Once the component is mounted, check if the notif should be visible
  useEffect(() => {
    (async () => {
      if (id != null) {
        const isVisible = await fetchNotifStatus(id);
        setIsNotifVisible(isVisible);
      }
    })();
  }, []);

  return isNotifVisible ? (
    <View>
      <Modal animationType="slide" transparent visible={isModalVisible}>
        <View style={styles.modal}>
          <View style={styles.modalContent}>
            <Text style={styles.notificationTitle}>{title}</Text>
            <Text style={styles.notificationBody}>{description}</Text>
            <Button
              title="OK"
              color={Colors.primary}
              onPress={async () => { await deleteNotification(); }}
            />
          </View>
        </View>
      </Modal>
      <View style={{ ...styles.notificationIcon, backgroundColor: color }}>
        <TouchableOpacity onPressIn={() => { setIsModalVisible(true); }}>
          <AntDesign name={icon} size={44} color={Colors.white} />
        </TouchableOpacity>
      </View>
    </View>
  ) : null;
};

export default NotificationItem;
