import AsyncStorage from "@react-native-community/async-storage";

import Colors from "../../../../utils/colors";

export const getIconStyle = (type: string): [string, string] => {
    switch (type) {
        case 'Warning':
            return ['warning', Colors.notifYellow];
        case 'Danger':
            return ['exclamationcircle', Colors.notifRed];
        case 'Reminder':
            return ['clockcircle', Colors.notifGreen];
        case 'Info':
            return ['questioncircle', Colors.notifBlue];
        default:
            return ['questioncircle', Colors.notifWhite];
    }
};

export const storeGlobalDelete = async (delId: number): Promise<void> => {
    try {
        const jsonValue = await AsyncStorage.getItem('deletedNotifications');
        const deletedNotifications = jsonValue !== null ? JSON.parse(jsonValue) : null;
        deletedNotifications.push(delId);

        await AsyncStorage.setItem('deletedNotifications', JSON.stringify(deletedNotifications));
    } catch (error) {
        await AsyncStorage.setItem('deletedNotifications', JSON.stringify([delId]));
        // console.log('the cache item probably did not exist previously or was corrupted', error);
    }
};

// Check if notif was hidden
export const fetchNotifStatus = async (delId: number): Promise<boolean> => {
    try {
        const jsonValue = await AsyncStorage.getItem('deletedNotifications');
        const deletedNotifications = jsonValue !== null ? JSON.parse(jsonValue) : null;
        if (deletedNotifications.includes(delId)) {
            return false;
        }
    } catch (error) {
      // console.log('error fetching notif status');
    }
    return true;
};