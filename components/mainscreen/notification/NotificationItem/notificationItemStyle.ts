import { StyleSheet, Dimensions } from 'react-native';

import Colors from '../../../../utils/colors';

export default StyleSheet.create({
    modal: {
        alignItems: 'center',
        top: Dimensions.get('window').height * 0.15,
    },
    modalContent: {
        alignItems: 'center',
        backgroundColor: Colors.white,
        borderRadius: 15,
        height: Dimensions.get('window').height * 0.60,
        justifyContent: 'space-evenly',
        padding: 20,
        width: Dimensions.get('window').width * 0.90,
    },
    notificationBody: {
        color: Colors.notifBody,
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    notificationIcon: {
        borderRadius: 7,
        fontSize: 24,
        height: 50,
        marginVertical: 5,
        opacity: 0.9,
        padding: 3,
        textAlign: 'center',
        textAlignVertical: 'center',
        width: 50,
    },
    notificationTitle: {
        color: Colors.notifTitle,
        fontSize: 24,
        textAlign: 'center',
        textAlignVertical: 'top',
    },
});