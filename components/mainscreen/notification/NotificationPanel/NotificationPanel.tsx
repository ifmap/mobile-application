import React from 'react';
import { FlatList } from 'react-native';

import NotificationItem from '../NotificationItem/NotificationItem';
import styles from './notificationPanelStyle';
import { Reminder } from '../../../../store/reminder/types';

interface NotificationPanelProps {
  notifications: Reminder[];
}

/** The container of each notification to display on the main screen */
const NotificationPanel: React.FC<NotificationPanelProps> = ({ notifications }) => {
  /** Sorts the notifications before displaying them
   *  Higher severity/oldest first. 1 is the highest priority 
   */
  const sortedNotifications = notifications.sort((a, b) => {
    if (a.severity < b.severity) return -1;
    if (a.sentDate < b.sentDate && a.severity === b.severity) return -1;
    return 0;
  });

  // Render flatlist  of notification items (list with not everything rendered and key-value pairs)
  return (
    <FlatList
      style={styles.notificationList}
      data={sortedNotifications.filter(N => N.sentDate < Date.now())}
      keyExtractor={(n) => (`${n.id}`)}
      renderItem={({ item }) => {
        return item.title && item.body ? (
          <NotificationItem
            id={item.id}
            title={item.title}
            type={item.type}
            description={item.body}
            isDeletable={item.isDeletable}
          />
        ) : null
      }}
    />
  );
};

export default NotificationPanel;
