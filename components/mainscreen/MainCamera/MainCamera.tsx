import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  Dimensions,
  Platform,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';
import { BarCodeScanningResult } from 'expo-camera/build/Camera.types';

import Icon from '../../ui/DynamicIcon/DynamicIcon';
import styles from './mainCameraStyle';

interface MainCameraProps {
  setIsModalVisible: (b: boolean) => void;
  setPickedImage: (s: string) => void;
}

const MainCamera: React.FC<MainCameraProps> = ({ setIsModalVisible, setPickedImage }) => {
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [flashMode, setFlashMode] = useState<string | number>(Camera.Constants.FlashMode.off);
  const [ratio, setRatio] = useState<string | undefined>();
  const [cameraHeight, setCameraHeight] = useState<string | undefined>();
  const [isSnapping, setIsSnapping] = useState<boolean>(false);
  const [isBarcodeScan, setIsBarcodeScan] = useState<boolean>(false);
  const [cameraZoom, setCameraZoom] = useState<number>(0);
  const [extraToggled, setExtraToggled] = useState<boolean>(false);
  const camera = useRef<Camera>(null);
  let isSubscribed = true;

  // Check the barcode data
  const processBarCode = (data: BarCodeScanningResult): void => {
    if (isBarcodeScan) {
      console.log(data);
    }
  };

  /** Function to handle taking a picture and display it */
  const snap = async (): Promise<void> => {
    if (camera.current) {
      setIsSnapping(true);
      const photo = await camera.current.takePictureAsync({ quality: 0.8 });
      const croppedPhoto = await ImageManipulator.manipulateAsync(photo.uri, [{
        crop: {
          originX: photo.width * 0.175,
          originY: photo.height * 0.20,
          width: photo.width * 0.65,
          height: photo.width * 0.65,
        },
      }]);

      setPickedImage(croppedPhoto.uri);
      setIsSnapping(false);
      setIsModalVisible(true);
    }
  };
  
  /** Function to select an existing picture to send */
  const selectPicture = async (): Promise<void> => {
    const permissionResponse = await Permissions.getAsync(Permissions.CAMERA);
    if (permissionResponse.status !== 'granted') {
      Alert.alert(
        'Permissions needed',
        'Please enable camera roll permissions in order to upload a picture from your gallery. You can go in your phone\'s settings to enable them.',
      );
    } else {
      const pickerResponse = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        quality: 0.55,
        aspect: [1, 1],
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
      });

      if ('uri' in pickerResponse) {
        setPickedImage(pickerResponse.uri);
        setIsModalVisible(true);
      }
    }
  };

  /** Function to determine the given ratio of the camera and how to display it to prevent the image from looking stretched */
  const prepareRatios = async (): Promise<void> => {
    if (Platform.OS === 'android' && camera.current) {
      const ratios = await camera.current.getSupportedRatiosAsync();

      // Get the maximum supported ratio for the current device
      // Usually the last element of "ratios" is the maximum supported ratio
      const selectedRatio = ratios[ratios.length - 1];
      setRatio(selectedRatio);

      const screenHeight = Dimensions.get('window').height;
      const screenWidth = Dimensions.get('window').width;
      const [hR, wR] = selectedRatio.split(':');

      let height = (screenWidth / parseInt(wR, 10)) * parseInt(hR, 10) / screenHeight * 100;
      if (height > 100) {
        height = 100;
      }
      setCameraHeight(`${height}%`);
    }
  };

  /** Toggles various flash modes */
  const changeFlash = (): void => {
    if (!isSubscribed) { 
      return;
    }
    switch(flashMode) {
      case Camera.Constants.FlashMode.off:
        setFlashMode(Camera.Constants.FlashMode.on);
        break;
      case Camera.Constants.FlashMode.on:
        setFlashMode(Camera.Constants.FlashMode.auto);
        break;
      default:
        setFlashMode(Camera.Constants.FlashMode.off);
        break;
    }
  };

  const flashIconName = (): string => {
    switch(flashMode) {
      case Camera.Constants.FlashMode.off:
        return 'flash-off';
      case Camera.Constants.FlashMode.on:
        return 'flash-on';
      default:
        return 'flash-auto';
    }
  };

  // Clean-up for no-ops
  useEffect(() => {
    return () => {
      isSubscribed = false;
    };
  });
  
  return (
    <Camera
      ref={camera}
      onCameraReady={prepareRatios}
      ratio={ratio}
      type={type}
      flashMode={flashMode}
      onBarCodeScanned={(data) => processBarCode(data)}
      zoom={cameraZoom}
      style={{ ...styles.camera, maxHeight: cameraHeight }}
    >
      {/* Picture frame */}
      <View style={styles.viewBox} />
      {/* Extra buttons */}
      <View style={styles.extraContainer}>
        <Icon
          onPressHandler={() => { setExtraToggled(!extraToggled); }}
          name={extraToggled ? 'chevron-up' : 'chevron-down'}
          type="Feather"
        />
        {/* Barcode toggle button */}
        {extraToggled && (
          <Icon
            onPressHandler={() => { setIsBarcodeScan(!isBarcodeScan); }}
            name="barcode-scan"
            type="MaterialCommunityIcons"
          >
            {!isBarcodeScan && (
              <Ionicons name="md-close" size={40} style={styles.txtIcon} />
            )}
          </Icon>
        )}
        {/* Image upload button */}
        {extraToggled && (
          <Icon
            onPressHandler={async () => { await selectPicture(); }}
            name="upload"
            type="MaterialCommunityIcons"
          />
        )}
      </View>
      {/* Footer buttons */}
      <View style={styles.bottomButtons}>
        {/* Camera Flip button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => {
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back,
            );
          }}
          name="camera-switch"
          type="MaterialCommunityIcons"
        />
        {/* FlashMode */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={changeFlash}
          name={flashIconName()}
          type="MaterialIcons"
          size={25}
        />
        {/* Take picture button */}
        {!isSnapping ? (
          <Icon
            touchableStyle={styles.btn}
            onPressHandler={async () => { await snap(); }}
            name="camera-iris"
            type="MaterialCommunityIcons"
            iconStyle={styles.icon}
          />
        ) : (
          <View style={styles.btn}>
            <ActivityIndicator style={styles.icon} size="large" color="#000000" />
          </View>
        )}
        {/* Zoom out button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => { setCameraZoom(cameraZoom - 0.1 < 0 ? 0 : cameraZoom - 0.1); }}
          name="zoom-out"
          type="Feather"
        />
        {/* Zoom in button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => { setCameraZoom(cameraZoom + 0.1 > 1 ? 1 : cameraZoom + 0.1); }}
          name="zoom-in"
          type="Feather"
        />
      </View>
    </Camera>
  );
};

export default MainCamera;
