import { StyleSheet, Dimensions } from 'react-native';

import Colors from '../../../utils/colors';

export default StyleSheet.create({
    bottomButtons: {
        backgroundColor: Colors.transparent,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    btn: {
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginBottom: 35,
        opacity: 0.8,
    },
    camera: {
        flex: 1,
        width: '100%',
    },
    extraContainer: {
        alignItems: 'flex-end',
        flex: 1,
        flexDirection: 'column-reverse',
        justifyContent: 'flex-start',
        right: 20,
    },
    icon: {
        alignSelf: 'flex-end',
        backgroundColor: Colors.accent,
        borderRadius: 35,
        elevation: 4,
        height: 70,
        opacity: 0.8,
        overflow: 'hidden',
        textAlign: 'center',
        textAlignVertical: 'center',
        width: 70,
    },
    txtIcon: {
        alignSelf: 'center',
        color: Colors.accent,
        fontSize: 40,
        marginBottom: 10,
        position: 'absolute',
    },
    viewBox: {
        alignSelf: 'center',
        borderColor: Colors.white,
        borderRadius: 10,
        borderWidth: 3,
        height: Dimensions.get('window').width * 0.65,
        justifyContent: 'center',
        position: 'relative',
        top: '20%',
        width: Dimensions.get('window').width * 0.65,
    },
});