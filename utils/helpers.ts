import { AsyncStorage, Alert } from 'react-native';
import * as Font from 'expo-font';

/** Toggles the guest mode */
export const storeGuest = async (isOn: string): Promise<void> => {
    try {
        await AsyncStorage.setItem('guestMode', isOn);
    } catch (error) {
        Alert.alert(
            'Error',
            'Error occured saving guest mode',
            [{ text: 'OK' }],
            { cancelable: false },
        );
    }
};

/** Fetches all the fonts needed before the app loads */
export const fetchFonts = (): Promise<void> => Font.loadAsync({
    'open-sans': require('../assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('../assets/fonts/OpenSans-Bold.ttf'),
    'audiowide-regular': require('../assets/fonts/Audiowide-Regular.ttf'),
    'arsenal-regular': require('../assets/fonts/Arsenal-Regular.ttf'),
    'poppins-regular': require('../assets/fonts/Poppins-Regular.ttf'),
    'poppins-bold': require('../assets/fonts/Poppins-Bold.ttf'),
});
