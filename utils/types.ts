import { FridgeItem } from "../store/fridge/types";
import { Reminder } from '../store/reminder/types';
import { FridgeGroup } from "../store/fridgeGroup/types";

export interface Notification {
    id: number;
    title: string;
    body: string;
    type: string;
    severity: number;
    sentDate: number;
    expirationDate: number;
    isDeletable: boolean;
    scope: string | null;
}

export interface FridgeItemType {
    id?: number;
    type: string;
    name: string;
    date?: number;
    expiryDate?: number;
    status?: string;
}

export interface FridgeGroupType {
    id?: number;
    name: string;
}

export interface ReminderItemType {
    id?: number;
    type: string;
    sentDate: number;
}

export interface UserDataObject { 
    fridgeItems?: FridgeItem[];
    fridgeGroups?: FridgeGroup[];
    reminders?: Reminder[];
    nextFridgeItemId?: number;
    nextFridgeGroupId?: number;
    nextReminderId?: number;
}