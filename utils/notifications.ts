import * as Expo from 'expo';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';

import { Reminder } from '../store/reminder/types';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: true,
    }),
});
  
export const requestPermissions = async (): Promise<void> => {
    const result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

    if (Constants.isDevice && result.status === 'granted') {
        console.log('Notification permissions granted.');
    }
}

export const addNotification = async (title: string, body: string, secondsFromNow: number): Promise<void> => {
    await Notifications.scheduleNotificationAsync({
        content: {
            title,
            body
        },
        trigger: {
            seconds: secondsFromNow,
        },
    });
}

export const removeNotification = async (id: string): Promise<void> => await Notifications.cancelScheduledNotificationAsync(id);

export const removeAllNotifications = async (): Promise<void> => await Notifications.cancelAllScheduledNotificationsAsync();

export const restoresRemindersAsync = async (reminders: Reminder[]): Promise<void> => { 
    await removeAllNotifications();
    for (let r = 0; r < reminders.length; r++) { 
      if (reminders[r].sentDate > Date.now()) {
        await addNotification("IFMAP Custom Reminder", reminders[r].title ?? "A fruit's reminder has been triggered", (reminders[r].sentDate - Date.now()) / 1000);
      }
    }
  }