import React, { useState } from 'react';
import {YellowBox} from 'react-native';
import {decode, encode} from 'base-64';
import { AppLoading } from 'expo';
import * as firebase from 'firebase';
import "firebase/auth";
import { enableScreens } from 'react-native-screens';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import MainNavigator from './navigation/MainNavigator';
import GuestNavigator from './navigation/GuestNavigator';
import fridgeReducer from './store/fridge/reducer';
import reminderReducer from './store/reminder/reducer';
import fridgeGroupReducer from './store/fridgeGroup/reducer';
import { storeGuest, fetchFonts } from './utils/helpers';
import { firebaseConfig } from './firebase.config';

// Fix polyfill atob/btoa
declare const global: NodeJS.Global & { atob: (a: string) => string, btoa: (b: string) => string};
if (!global.btoa) { global.btoa = encode }
if (!global.atob) { global.atob = decode } 

// Fix timer warnings on Android because of firebase
const ignoredWarnings = ['Setting a timer'];
YellowBox.ignoreWarnings(ignoredWarnings);
console.warn = (message: string) => {
  if (!new RegExp(ignoredWarnings.join("|")).test(message)) {
    console.warn.apply(message);
  }
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

/** Activate the screens */
enableScreens();

// Store
const rootReducer = combineReducers({
  fridge: fridgeReducer,
  reminders: reminderReducer,
  fridgeGroups: fridgeGroupReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, applyMiddleware(thunk));

const App: React.FC = () => {
  const fireAuth: firebase.auth.Auth = firebase.auth();
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [isLoggedIn, setIsLoggedIn] = useState<firebase.User | null>(fireAuth.currentUser);

  /** Toggles guest mode depending on auth state */
  fireAuth.onAuthStateChanged((user): void  => {
    storeGuest(user !== null ? 'false' : 'true');
    setIsLoggedIn(user);
  });
  
  // Waits for everything to be fetched
  if (!isLoaded) {
    return <AppLoading startAsync={fetchFonts} onFinish={() => setIsLoaded(true)} />;
  }
  
  // Once loaded, render the app
  return (
    <Provider store={store}>
      {isLoggedIn ? <MainNavigator /> : <GuestNavigator />}
    </Provider>
  )
}

export default App;
